/*
 * grunt-image-manifest
 * https://github.com/djeckhart/grunt-image-manifest
 *
 * Copyright (c) 2015 Danny Eckhart
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      // tests: ['tmp']
    },

    // Configuration to be run
    image_manifest: {
      dev: {
        options: {
        },
        files: [{
          expand: true,     // Enable dynamic expansion.
          src: ['fixtures/**/*.{jpg,gif,png}'],
          dest: 'image-manifest',
          ext: '.yml',   // Dest filepaths will have this extension.
        }],
      },
    },

    watch: {
      image_manifest: {
        files: 'tasks/image_manifest.js',
        tasks: ['default']
      },
    },

  });

  // Load this plugin's task(s).
  grunt.loadTasks('tasks');

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-debug-task');

  grunt.registerTask('default', ['jshint', 'image_manifest']);

};
