/*
 * grunt-image-manifest
 * https://github.com/djeckhart/grunt-image-manifest
 *
 * Copyright (c) 2015 Danny Eckhart
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  var _ = require('lodash');
  var async = require('async');
  var gm = require('gm');
  var fs = require('fs');
  var path = require('path');
  var util = require('util');
  var yaml = require('js-yaml');

  var imagesData = [];
  var tally = 0;
  var DEFAULT_OPTIONS = {
    engine: 'gm', // gm or im
  };

  // details about the GFX rendering engines being used
  var GFX_ENGINES = {
    im: {
      name: 'ImageMagick',
      brewurl: 'imagemagick',
      url: 'http://www.imagemagick.org/script/binary-releases.php',
      alternative: {
        code: 'gm',
        name: 'GraphicsMagick'
      }
    },
    gm: {
      name: 'GraphicsMagick',
      brewurl: 'graphicsmagick',
      url: 'http://www.graphicsmagick.org/download.html',
      alternative: {
        code: 'im',
        name: 'ImageMagick'
      }
    }
  };

  var cache = {},
    gfxEngine = {};

  /**
   * Set the engine to ImageMagick or GraphicsMagick
   *
   * @private
   * @param  {string}          engine     im for ImageMagick, gm for GraphicsMagick
   */
  var getEngine = function(engine) {
    if (typeof GFX_ENGINES[engine] === 'undefined') {
      return grunt.fail.warn('Invalid render engine specified');
    }
    grunt.verbose.ok('Using render engine: ' + GFX_ENGINES[engine].name);

    if (engine === 'im') {
      return gm.subClass({
        imageMagick: (engine === 'im')
      });
    }

    return gm;
  };


  /**
   * Handle showing errors to the user.
   *
   * @private
   * @param   {string}          error The error message.*@param {
         string
       }    engine    The graphics engine being used.
   */
  var handleImageErrors = function(error, engine) {
    if (error.message.indexOf('ENOENT') > -1) {
      grunt.log.error(error.message);

      grunt.fail.warn('\nPlease ensure ' + GFX_ENGINES[engine].name + ' is installed correctly.\n' +
        '`brew install ' + GFX_ENGINES[engine].brewurl + '` or see ' + GFX_ENGINES[engine].url + ' for more details.\n' +
        'Alternatively, set options.engine to \'' + GFX_ENGINES[engine].alternative.code + '\' to use ' + GFX_ENGINES[engine].alternative.name + '.\n');
    } else {
      grunt.fail.warn(error.message);
    }
  };

  var processImage = function(imagePath, callback) {
    var image = gfxEngine(imagePath);
    image.size(function(err, size) {
      if (err) {
        grunt.log.writeln(err);
        callback();
      } else {
        // Get a clean vesion of the image filename name to use as a key
        var extension = path.extname(imagePath);
        var imageKey = path.basename(imagePath, extension);
        var imageInfo = {
          src: path.basename(imagePath),
          'data-size': size.width + 'x' + size.height,
        };

        imagesData.push([imageKey, imageInfo]);
        callback();
      }
    });
    tally++;
  };

  var outputResult = function(destinationPath) {
    var yamlString = yaml.safeDump(imagesData);
    fs.writeFile(destinationPath, yamlString, function(err) {
      if (err) {
        grunt.log.writeln('%f', err);
      }
    });

    grunt.log.writeln('Wrote info for ' + tally.toString() + ' images to ' + destinationPath + '.');
  };

  var findAndPushImage = function(imagePath, series) {
    if (typeof imagePath === 'string') {
      // grunt.log.writeln('Pushing task for' + file);
      series.push(function(callback) {
        return processImage(imagePath, callback);
      });

    } else {
      grunt.log.writeln('Unable to load image %f', imagePath);
    }
  };

  grunt.registerMultiTask('image_manifest', 'Read a directory of images and generate YAML for image galleries', function() {

    // Tell Grunt this task is asynchronous.
    var done = this.async();
    var task = this;
    var series = [];
    var options = task.options(DEFAULT_OPTIONS); // Merge task-specific and/or target-specific options with these defaults.

    gfxEngine = getEngine(options.engine);

    // Iterate over all specified file groups.
    task.files.forEach(function(file) {

      var imagePath = '';
      var arrayLength = file.src.length;

      for (var i = 0; i < arrayLength; i++) {
        imagePath = file.src[i];
        findAndPushImage(imagePath, series);
      }

    });

    // Gets pushed last, gets executed past.
    series.push(function(callback) {
      outputResult('image_manifest.yml');
      return callback();
    });


    async.series(series, done);
  });

};
